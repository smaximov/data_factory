defmodule DataFactory do
  @moduledoc """
  Documentation for `DataFactory`.
  """

  use Application

  @callback factory(term, map) :: map
  @callback next(term) :: term

  @impl Application
  def start(_type, _args), do: DataFactory.Sequence.start_link()

  defmacro __using__(_opts) do
    quote do
      alias DataFactory.Builder

      import DataFactory.Compiler, only: [factory: 2, sequence: 2]

      @behaviour DataFactory
      @before_compile DataFactory.Compiler

      @impl DataFactory
      def factory(factory_name, overrides)

      @impl DataFactory
      def next(sequence_name)

      defdelegate next(sequence_name, formatter), to: Builder

      @spec build(term, map | keyword) :: map
      def build(factory_name, overrides \\ []) do
        Builder.build(__MODULE__, factory_name, overrides)
      end
    end
  end
end
