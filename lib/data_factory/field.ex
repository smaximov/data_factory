defmodule DataFactory.Field do
  @moduledoc false

  @type t() :: %__MODULE__{
          name: atom(),
          label: term(),
          deps: [atom()],
          ast: Macro.t()
        }

  @enforce_keys [:name, :label, :deps, :ast]
  defstruct [:name, :label, :deps, :ast]

  @spec from_ast(Macro.t()) :: t()
  def from_ast({:field, _meta1, [{name, _meta2, context}, opts]})
      when is_atom(name) and is_atom(context) and is_list(opts) do
    deps = build_deps(List.wrap(opts[:require]))
    label = Keyword.get(opts, :as, name)

    %__MODULE__{name: name, label: label, deps: deps, ast: opts[:do]}
  end

  def from_ast({:field, _meta1, [{name, _meta2, context}, opts, body]})
      when is_atom(name) and is_atom(context) and is_list(opts) and is_list(body) do
    deps = build_deps(List.wrap(opts[:require]))
    label = Keyword.get(opts, :as, name)

    %__MODULE__{name: name, label: label, deps: deps, ast: body[:do]}
  end

  def from_ast({:field, _meta1, [{name, _meta2, context}]})
      when is_atom(name) and is_atom(context) do
    %__MODULE__{name: name, label: name, deps: [], ast: nil}
  end

  defp build_deps(deps) when is_list(deps) do
    Enum.map(deps, fn {name, _meta, context} when is_atom(name) and is_atom(context) -> name end)
  end
end
