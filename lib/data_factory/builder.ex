defmodule DataFactory.Builder do
  alias DataFactory.Sequence

  @spec build(module, term, map | keyword) :: map
  def build(factory, factory_name, overrides)
      when is_atom(factory) and (is_map(overrides) or is_list(overrides)) do
    overrides = Map.new(overrides)

    factory.factory(factory_name, overrides)
  end

  @spec next(term, (integer -> result)) :: result when result: term
  def next(sequence_name, formatter) when is_function(formatter, 1) do
    sequence_name
    |> Sequence.next()
    |> formatter.()
  end
end
