defmodule DataFactory.Sequence do
  @moduledoc false

  use GenServer

  @spec start_link(named_table?: :boolean) :: GenServer.on_start()
  def start_link(opts \\ []), do: GenServer.start_link(__MODULE__, opts)

  @default_table_opts [:set, :public, write_concurrency: true]

  @impl GenServer
  def init(opts) do
    named_table? = Keyword.get(opts, :named_table?, true)

    table_opts =
      if named_table? do
        [:named_table | @default_table_opts]
      else
        @default_table_opts
      end

    table = :ets.new(__MODULE__, table_opts)

    {:ok, table}
  end

  @spec next(:ets.tab(), term()) :: integer()
  def next(table \\ __MODULE__, sequence_name) do
    :ets.update_counter(table, sequence_name, 1, {sequence_name, 0})
  end
end
