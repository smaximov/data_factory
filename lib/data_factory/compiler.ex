defmodule DataFactory.Compiler do
  @moduledoc false

  alias DataFactory.Field

  defmacro factory(name, do: {:__block__, _meta, clauses}) when is_list(clauses) do
    fields = Enum.map(clauses, &Field.from_ast/1)

    compile_factory(name, fields)
  end

  defmacro factory(name, [{:do, clause = {:field, _meta, field_def}}])
           when is_list(field_def) do
    fields = [Field.from_ast(clause)]

    compile_factory(name, fields)
  end

  defmacro sequence(sequence_name, formatter) do
    quote do
      def next(unquote(sequence_name)) do
        DataFactory.Builder.next(unquote(sequence_name), unquote(formatter))
      end
    end
  end

  defp compile_factory(name, fields) do
    fields_initialization_order = sort_field_deps(fields)
    fields_index = Map.new(fields, &{&1.name, &1})

    overrides = Macro.var(:overrides, nil)

    fields_initialization =
      Enum.map(fields_initialization_order, fn field_name ->
        initialize_field(overrides, Map.fetch!(fields_index, field_name))
      end)

    quote do
      def factory(unquote(name), unquote(overrides)) do
        unquote(overrides) = Map.new(unquote(overrides))
        unquote_splicing(fields_initialization)
        unquote(collect_fields(fields))
      end
    end
  end

  defp sort_field_deps(fields) do
    graph = :digraph.new(~w[private]a)

    try do
      for field <- fields do
        # TODO: handle errors
        :digraph.add_vertex(graph, field.name)
      end

      for field <- fields,
          dep <- field.deps do
        # TODO: handle errors
        [:"$e" | _] = :digraph.add_edge(graph, dep, field.name)
      end

      :digraph_utils.topsort(graph)
    after
      :digraph.delete(graph)
    end
  end

  defp initialize_field(overrides, field) do
    quote do
      unquote(Macro.var(field.name, nil)) =
        Map.get_lazy(unquote(overrides), unquote(field.name), fn -> unquote(field.ast) end)
    end
  end

  defp collect_fields(fields) do
    fields = Enum.map(fields, &{&1.label, Macro.var(&1.name, nil)})

    {:%{}, [], fields}
  end

  defmacro __before_compile__(_env) do
    quote do
      def factory(factory_name, _overrides) do
        # TODO: custom exceptions
        raise """
        No factory defined for #{inspect(factory_name)}.

        Please check for typos or define your factory:

            factory #{inspect(factory_name)} do
              # ...
            end
        """
      end

      def next(sequence_name) do
        DataFactory.Sequence.next(sequence_name)
      end
    end
  end
end
