{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
      in {
        devShell = pkgs.mkShell {
          name = "data_factory";

          buildInputs = with pkgs; [
            elixir
            elixir_ls

            erlang
          ];

          ERL_AFLAGS = "-kernel shell_history enabled";
          TERM = "xterm-256color";
        };
      }
    );
}
